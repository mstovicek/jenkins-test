<?php

namespace App;

class Index
{
    public function sayHello($name)
    {
        $loader = new \Twig_Loader_Array(
            [
                'index' => 'Hello {{ name }}!',
            ]
        );

        $twig = new \Twig_Environment($loader);

        return $twig->render('index', ['name' => $name]);
    }
}
