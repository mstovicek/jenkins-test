<?php

namespace spec\App;

use App\Index;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class IndexSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Index::class);
    }

    function it_says_hello()
    {
        $this->sayHello('Milane')->shouldBe('Hello Milane!');
    }
}
